package com.example.test;
import com.android.volley.RequestQueue;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import com.android.volley.VolleyError;
import com.android.volley.Response;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class UserActivity extends AppCompatActivity {
    RequestQueue queue ;//= Volley.newRequestQueue(UserActivity.this);
    String urllock ="http://192.168.43.221:3000/lock";
    String urlunlock ="http://192.168.43.221:3000/unlock";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        queue = Volley.newRequestQueue(UserActivity.this);
        Button btnlock = (Button) findViewById(R.id.button1);
        Button btnunlock = (Button) findViewById(R.id.button2);
        btnlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
// Request a string response from the provided URL.
               StringRequest stringRequest = new StringRequest(Request.Method.GET, urllock,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Display the first 500 characters of the response string.
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });

// Add the request to the RequestQueue.
                queue.add(stringRequest);
            }
        });
        btnunlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
// Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(Request.Method.GET, urlunlock,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Display the first 500 characters of the response string.
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });

// Add the request to the RequestQueue.
                queue.add(stringRequest);

            }
        });
    }
}
